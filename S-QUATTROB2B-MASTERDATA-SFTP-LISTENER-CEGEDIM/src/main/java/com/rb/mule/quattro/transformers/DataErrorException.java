package com.rb.mule.quattro.transformers;

import org.mule.api.transformer.TransformerException;
import org.mule.config.i18n.Message;

public class DataErrorException extends TransformerException {

	private static final long serialVersionUID = 6496329132883126504L;

	public DataErrorException(Message s){
		super(s);
	}
	
}
