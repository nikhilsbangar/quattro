package com.rb.mule.quattro.salesforcePojo;

public class ProductPrice {
	private String priceListId;
	private String prodId;
	private String price;
	private String disc_1;
	private String pharId;
	
	public String getPriceListId() {
		return priceListId;
	}
	public void setPriceListId(String priceListId) {
		this.priceListId = priceListId;
	}
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDisc_1() {
		return disc_1;
	}
	public void setDisc_1(String disc_1) {
		this.disc_1 = disc_1;
	}
	public String getPharId() {
		return pharId;
	}
	public void setPharId(String pharId) {
		this.pharId = pharId;
	}
	
}
