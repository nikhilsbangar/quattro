package com.rb.mule.quattro.transformers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rb.mule.pojos.FileEntry;

/**
 *  <p>Class converts a list of FileEntry beans into a map Map&lt;String,FileEntry&gt;
 * 
 *  <br>The key for each entry is taken from fileEntryACR (which is a file name to the last underscore)  
 * 
 *  @author Stanislaw Antoniak, ICC, RB, stanislaw.antoniak@rb.com
 */
public class FileEntryCollection2FileEntryMap extends AbstractTransformer {

	protected Logger LOGGER = LoggerFactory.getLogger(FileEntryCollection2FileEntryMap.class);

	/**
	 * Transforms  a list of FileEntry beans into a map Map&lt;String,FileEntry&gt;
	 * @param src A list containing FileEntry beans 
	 * @param encoding Put UTF-8 here.
	 * @return Ordered map od FileEntry beans ( Map&lt;String,FileEntry&gt;) with key taken from getFileNameACR() 
	 */
	@Override
	public Object doTransform(Object src, String encoding) throws TransformerException{

		@SuppressWarnings("unchecked")
		List<FileEntry> filesCollection = (List<FileEntry>) src; 

		Map<String, FileEntry> filesMap = new TreeMap<String,FileEntry>();
		
		for (FileEntry file : filesCollection){

			//String key = file.getFileNameACR();
			String key = file.getFileName();
			
			LOGGER.trace("File "+file.getFileName()+" saved to map as "+key);
			
			filesMap.put(key, file);
		}

		return filesMap;
	}

}
