package com.rb.mule.quattro.transformers;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVUtils;
import org.mule.api.transformer.TransformerException;
import org.mule.config.i18n.CoreMessages;
import org.mule.transformer.AbstractTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rb.mule.quattro.salesforcePojo.Discount;
import com.rb.mule.utils.MemoryLogger;

public class DiscountFileMerge extends AbstractTransformer {
	protected Logger LOGGER = LoggerFactory.getLogger(DiscountFileMerge.class);
	
	private MemoryLogger memory = new MemoryLogger();
	
	@Override
	protected ArrayList<Discount> doTransform(Object src, String enc) throws TransformerException {
		List<String> rows = new ArrayList<String>();
		rows = converttoarray(src.toString());

		ArrayList<Discount> discountCollection = new ArrayList<Discount>();
		
		for (String line : rows) {
			String[] row = null;

			try {
				row = CSVUtils.parseLine(line);
				Discount p = new Discount();
				StringBuilder sb = new StringBuilder();
				//System.out.println(row[0]+" : "+row[1]+" : "+row[2]+" : "+row[3]+" : "+row[4]);

				sb = new StringBuilder();
				p.setPharId(sb.append(row[0].trim()).toString());
				
				sb = new StringBuilder();
				p.setProdId(sb.append(row[1].trim()).toString());
				
				
				sb = new StringBuilder();
				p.setDisc_1(sb.append(row[2].trim()).toString());
				
				
				discountCollection.add(p);
				
			}catch (Exception e) {
				StringBuilder rowForLog = new StringBuilder();
				for (int i = 0; i < row.length; i++)
					rowForLog.append(row[i]).append("::");

				LOGGER.error("Error parsing ES_Discount file. " + rowForLog.toString());
				throw new TransformerException(
						CoreMessages.createStaticMessage("Error parsing ES_Discount file. " + rowForLog.toString() + e));
			} 
		}	
		
		LOGGER.info("Memory status " + MemoryLogger.getMemoryForLogging());
		return discountCollection;
	}
	
	public List<String> converttoarray(String value) {

		List<String> entries = new ArrayList<String>();
		int start = 1;
		while (true) {
			start = value.indexOf("[", start);
			int end = value.indexOf("]", start);
			if (start != -1 && end != -1) {
				entries.add(value.substring(start+1, end));
				start = end + 1;
			} else {
				break;
			}
		}
		
		return entries;

	}


}
