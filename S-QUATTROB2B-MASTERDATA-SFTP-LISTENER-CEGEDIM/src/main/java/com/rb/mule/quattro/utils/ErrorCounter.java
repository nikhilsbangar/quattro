package com.rb.mule.quattro.utils;

public class ErrorCounter {
	
	 int errorCounter = 0;
	 
	 public int increment()
	 {
	    errorCounter++;
	    return errorCounter;
	 }
}
