package com.rb.mule.quattro.salesforcePojo;

public class Discount {
	private String prodId;
	private String disc_1;
	private String pharId;
	
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getDisc_1() {
		return disc_1;
	}
	public void setDisc_1(String disc_1) {
		this.disc_1 = disc_1;
	}
	public String getPharId() {
		return pharId;
	}
	public void setPharId(String pharId) {
		this.pharId = pharId;
	}
	
}
