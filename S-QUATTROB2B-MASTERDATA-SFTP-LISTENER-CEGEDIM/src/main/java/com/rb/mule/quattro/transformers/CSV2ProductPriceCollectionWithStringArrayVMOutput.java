package com.rb.mule.quattro.transformers;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.config.i18n.CoreMessages;
import org.mule.module.client.MuleClient;
import org.mule.transformer.AbstractMessageTransformer;
import org.mule.transport.vm.VMConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.rb.mule.utils.MemoryLogger;

public class CSV2ProductPriceCollectionWithStringArrayVMOutput extends AbstractMessageTransformer {
	protected Logger LOGGER = LoggerFactory.getLogger(CSV2ProductPriceCollectionWithStringArrayVMOutput.class);

	private char delimiter;
	private char quoteChar;
	private Integer skipLines;

	private String vmConnector;
	private String vmQueue;
	private int maxQueueSize;
	
	
	private MemoryLogger memory = new MemoryLogger();
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		List rowsChunk = new ArrayList();
		CSVReader reader = null;
		MuleClient mc;
		
		try {
			mc = new MuleClient(this.muleContext);
		} catch (MuleException e1) {
			throw new TransformerException(CoreMessages.createStaticMessage("Cannot create MuleClient"));
		}
		VMConnector vmConnectorObject = (VMConnector) muleContext.getRegistry().lookupConnector(this.vmConnector);
		
		try{
			StringReader sr = new StringReader(message.getPayloadAsString("UTF-8"));

			CSVParser csvParser = new CSVParserBuilder().withSeparator(this.delimiter)
					.withQuoteChar(this.quoteChar).build();

			reader = new CSVReaderBuilder(sr).withSkipLines(this.skipLines).withCSVParser(csvParser).build();
			
			LOGGER.info("Starting transform, CVS Reader initiated");

			int i = 0;
			int skipHeader = 0;
			String[] fields;
			while ((fields = reader.readNext()) != null) {
				
				if(skipHeader == 0){
					skipHeader=1;
					continue;
				}
					
				
				fields[3] = fields[3].replace(",", "");
				
				//System.out.println("Fields :: " + fields[0]+ "::" + fields[1]+ "::" + fields[2]+ "::" + fields[3]+ "::" + fields[4]);
				if(fields.length>0)
				rowsChunk.add(Arrays.asList(fields));

				if (rowsChunk.size() == 20000) {
					int size;
					while ((size = vmConnectorObject.getQueueManager().getQueueSession().getQueue(this.vmQueue)
							.size()) > this.maxQueueSize) {
						LOGGER.trace(this.vmQueue + " queue size: " + size + ", waiting 3s");
						Thread.sleep(1000);
					}
					MuleMessage msg = message.createInboundMessage();
					String payload = rowsChunk.toString();
					msg.setPayload(payload);
					mc.dispatch("vm://" + this.vmQueue, msg);

					rowsChunk = new ArrayList();

					System.gc();

					LOGGER.info(++i + " chunks processed. Memory status " + memory.getMemoryForLogging());	
				}
			}
			// Load the left over chunks into VM
				MuleMessage msg = message.createInboundMessage();
				String payload1 = rowsChunk.toString();
				msg.setPayload(payload1);
				mc.dispatch("vm://" + this.vmQueue, msg);

				System.gc();
				LOGGER.info(++i + " chunks processed. Memory status " + memory.getMemoryForLogging());
		} catch (IOException e) {
			throw new TransformerException(
					CoreMessages.createStaticMessage("Problem converting pipe delimited csv to String[] collection"));
		} catch (DataErrorException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error trace", e);

			throw new TransformerException(CoreMessages.createStaticMessage(
					"Problem sending output to VM queue " + this.vmQueue + "Exception caused at " + e));
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				// should not happen
				// files are probably correct so just print stacktrace to log
				// and continue
				LOGGER.error("streams for zip file not closed properly", e);
			}

		}
		return null;
	}
	
	public char getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	public char getQuoteChar() {
		return quoteChar;
	}

	public void setQuoteChar(char quoteChar) {
		this.quoteChar = quoteChar;
	}

	public Integer getSkipLines() {
		return skipLines;
	}

	public void setSkipLines(Integer skipLines) {
		this.skipLines = skipLines;
	}

	public String getVmConnector() {
		return vmConnector;
	}

	public void setVmConnector(String vmConnector) {
		this.vmConnector = vmConnector;
	}

	public String getVmQueue() {
		return vmQueue;
	}

	public void setVmQueue(String vmQueue) {
		this.vmQueue = vmQueue;
	}

	public int getMaxQueueSize() {
		return maxQueueSize;
	}

	public void setMaxQueueSize(int maxQueueSize) {
		this.maxQueueSize = maxQueueSize;
	}

	public MemoryLogger getMemory() {
		return memory;
	}

	public void setMemory(MemoryLogger memory) {
		this.memory = memory;
	}


}
