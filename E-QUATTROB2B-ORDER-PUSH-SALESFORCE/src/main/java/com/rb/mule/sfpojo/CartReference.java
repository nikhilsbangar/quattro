package com.rb.mule.sfpojo;

public class CartReference {
	
	private String cegedim_Order_Id__c;
	private String Id;
	public String getCegedim_Order_Id__c() {
		return cegedim_Order_Id__c;
	}
	public void setCegedim_Order_Id__c(String cegedim_Order_Id__c) {
		this.cegedim_Order_Id__c = cegedim_Order_Id__c;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		this.Id = id;
	}
	public void putCartRef(String cegedim_Order_Id__c, String id) {
		this.cegedim_Order_Id__c = cegedim_Order_Id__c;
		this.Id = id;
	}
	
}