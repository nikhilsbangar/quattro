package org.mule.utils;

public class ErrorCounter {
	
	 int errorCounter = 0;
	 
	 public int increment()
	 {
	    errorCounter++;
	    return errorCounter;
	 }
}
